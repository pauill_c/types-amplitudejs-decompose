# Installation
> `npm install --save @types/amplitude-js`

# Summary
This package contains type definitions for Amplitude SDK (https://github.com/amplitude/Amplitude-Javascript).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/amplitude-js.

### Additional Details
 * Last updated: Wed, 02 Dec 2020 12:01:16 GMT
 * Dependencies: none
 * Global values: `amplitude`

# Credits
These definitions were written by [Arvydas Sidorenko](https://github.com/Asido), [Dan Manastireanu](https://github.com/danmana), and [Kimmo Hintikka](https://github.com/HintikkaKimmo).
